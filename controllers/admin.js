const { validationResult } = require('express-validator');

const Product = require('../models/product');

const ITEMS_PER_PAGE = 2;

exports.getAddProduct = (req, res, next) => {
  res.render('admin/add-product', {
    validationErrors: [],
    errorMessage: '',
    pageTitle: 'Add Product',
    path: '/admin/add-product',

    title: '',
    imageUrl: '',
    price: '',
    description: ''
  });
};

exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422)
      .render('admin/add-product', {
        validationErrors: errors.array(),
        errorMessage: errors.array()[0].msg,    // show 1st element of the array at the top
        pageTitle: 'Add Product',
        path: '/admin/add-product',
        title: title,
        imageUrl: imageUrl,
        price: price,
        description: description
      });
  }

  const product = new Product({
    title: title,
    price: price,
    description: description,
    imageUrl: imageUrl,
    userId: req.user
  });
  product
    .save()
    .then(result => {
      res.status(201)
        .redirect('/admin/products');
    })
    .catch(err => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);

      // return res.status(500)
      //   .redirect('/500');

        // .render('admin/add-product', {
        //   validationErrors: [],
        //   errorMessage: 'Mongoose Error: ' + err.toString(),
        //   pageTitle: 'Add Product',
        //   path: '/admin/add-product',
          
        //   title: title,
        //   imageUrl: imageUrl,
        //   price: price,
        //   description: description
        // });
    });
};

exports.getEditProduct = (req, res, next) => {
  const editMode = req.query.edit;
  if (!editMode) {
    return res.redirect('/');
  } 
  const prodId = req.params.productId;
  Product.findById(prodId)
    .then(product => {
      if (!product) {
        return res.redirect('/');
      }
      res.render('admin/edit-product', {
        errorMessage: '',
        validationErrors: [],
        pageTitle: 'Edit Product',
        path: '/admin/edit-product',
        pageTitle: 'Edit Product',
        path: '/admin/edit-product',
        prodId: product._id,
        title: product.title,
        price: product.price,
        imageUrl: product.imageUrl,
        description: product.description
      });
    })
    .catch(
      err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
      });
};

exports.postEditProduct = (req, res, next) => {
  const prodId = req.body.productId;
  const updatedTitle = req.body.title;
  const updatedPrice = req.body.price;
  const updatedImageUrl = req.body.imageUrl;
  const updatedDesc = req.body.description;

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422)
      .render('admin/edit-product', {
        errorMessage: errors.array()[0].msg,
        validationErrors: errors.array(),
        pageTitle: 'Edit Product',
        path: '/admin/edit-product',
        prodId: prodId,
        title: updatedTitle,
        price: updatedPrice,
        imageUrl: updatedImageUrl,
        description: updatedDesc 
      });
  };

  Product.findById(prodId)
    .then(product => {
      if (product.userId.toString() !== req.user._id.toString()) {
        return res.redirect('/');
      }
      product.title = updatedTitle; 
      product.price = updatedPrice;
      product.description = updatedDesc;
      product.imageUrl = updatedImageUrl;
      return product.save()
        .then(result => {
          res.status(200)
            .redirect('/admin/products');
        });
    })
    .catch(
      err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
      }
    );
};

exports.getProducts = (req, res, next) => {
  let page = +req.query.page;
  let totProds = 0;
  if (isNaN(page) ) {
    page = 1; 
  }

  Product.countDocuments({ userId: req.user._id })
    .then(numProds => {
      totProds = numProds;
      return Product.find({ userId: req.user._id })
        .skip((page - 1) * ITEMS_PER_PAGE)
        .limit(ITEMS_PER_PAGE)
        .then(products => {
          res.render('admin/products', {
            prods: products,
            pageTitle: 'Admin Products',    
            path: '/admin/products',
            currPage: page,
            hasNext: page * ITEMS_PER_PAGE < totProds,  
            hasPrev: page > 1,
            nextPage: page + 1,
            prevPage: page - 1, 
            lastPage: Math.ceil(totProds / ITEMS_PER_PAGE)
          })
      })
    })
    .catch(error => {
      console.log(error);
    })

};

exports.postDeleteProduct = (req, res, next) => {
  const prodId = req.body.productId;
  Product.deleteOne({ _id: prodId, userId: req.user._id })
    .then(() => {
      res.status(200)
        .redirect('/admin/products');
    })
    .catch(
      err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
      }
    );
};
