const path = require('path');
const express = require('express');
const {body,check} = require('express-validator');

const adminController = require('../controllers/admin');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

// /admin/add-product => GET
router.get('/add-product', isAuth, adminController.getAddProduct);

// /admin/add-product => POST
router.post(
    '/add-product', 
    isAuth, 
    [
        body('title')
            .trim()
            .isString()
            .isLength({min: 2, max: 30})
            .withMessage('Title must be at least 2 characters long'),
        body('price')
            .isDecimal(),
        body('description')
            .isLength({min: 3, max: 200})
            .withMessage('Please enter at least 3 characters for description')
    ],
    adminController.postAddProduct
);

router.get('/edit-product/:productId', isAuth, adminController.getEditProduct);

router.post(
    '/edit-product', 
    isAuth, 
    [
        body('title')
            .trim()
            .isString()
            .isLength({min: 2, max: 30})
            .withMessage('Title must be at least 2 characters long'),
        body('price')
            .isDecimal(),
        body('description')
            .isLength({min: 3, max: 200})
            .withMessage('Please enter at least 3 characters for description')
    ],
    adminController.postEditProduct
);

router.post('/delete-product', isAuth, adminController.postDeleteProduct);

// /admin/products => GET
router.get('/products', isAuth, adminController.getProducts);

module.exports = router;
